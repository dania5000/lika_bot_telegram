import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.telegram.telegrambots.ApiContext;
import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.TelegramBotsApi;
import org.telegram.telegrambots.bots.DefaultBotOptions;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        ApiContextInitializer.init();
        DefaultBotOptions botOptions = ApiContext.getInstance(DefaultBotOptions.class);
        Bot bot = new Bot(botOptions);
        TelegramBotsApi telegramBotsApi = new TelegramBotsApi();

        try {
            telegramBotsApi.registerBot(bot);
        } catch (Exception e) {
        }

        Order order1 = null;
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Danil\\IdeaProjects\\projectLika\\src\\main\\resources\\chromedriver_win32\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.get("https://profi.ru/backoffice/n.php");
        StepOnSite step = new StepOnSite();
        step.authMethod(driver);
        while (true) {
            Order order = step.getOrder(driver);

            if (order1 != null && !order1.equals(order)) {
                System.out.println(order);
                try {
                    bot.sendMessage(order.toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            order1 = order;
        }
    }

}
