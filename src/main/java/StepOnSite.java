import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.util.Scanner;

public class StepOnSite {
    Scanner in = new Scanner(System.in);

    public void authMethod(WebDriver driver) throws InterruptedException {
        driver.findElement(By.xpath("//*[@id=\"content\"]/div/div[2]/div/div[2]/div/div/form/div/div/div[1]/label/span/input")).sendKeys("9207345940");
        Thread.sleep(2000);
        driver.findElement(By.xpath("//*[@id=\"content\"]/div/div[2]/div/div[2]/div/div/form/a")).click();
        String number1 = getNumber("Введите первое число: ");
        String number2 = getNumber("Введите второе число: ");
        String number3 = getNumber("Введите третье число: ");
        String number4 = getNumber("Введите четвертое число: ");
        driver.findElement(By.xpath("//*[@id=\"content\"]/div/div[2]/div/div[2]/div/div/div/div[1]/div[1]/input[1]")).sendKeys(number1);
        driver.findElement(By.xpath("//*[@id=\"content\"]/div/div[2]/div/div[2]/div/div/div/div[1]/div[1]/input[2]")).sendKeys(number2);
        driver.findElement(By.xpath("//*[@id=\"content\"]/div/div[2]/div/div[2]/div/div/div/div[1]/div[1]/input[3]")).sendKeys(number3);
        driver.findElement(By.xpath("//*[@id=\"content\"]/div/div[2]/div/div[2]/div/div/div/div[1]/div[1]/input[4]")).sendKeys(number4);
    }

    public Order getOrder(WebDriver driver) throws InterruptedException {
        Thread.sleep(3000);
        driver.navigate().refresh();

        Thread.sleep(5000);
        String name = "нет описания";
        try {
            name = driver.findElement(By.xpath("//div[2]/div/div/div/div[1]/div/a/div[1]/div[1]/p")).getText();
        } catch (Exception ignored) {
        }

        Thread.sleep(1000);
        String price = "нет описания";
        try {
            price = driver.findElement(By.xpath("//div[1]/div/a/div[2]/div[1]/div/div/div/span")).getText();
        } catch (Exception ignored) {
        }

        Thread.sleep(1000);
        String description = "нет описания";
        try {

            description = driver.findElement(By.xpath("//div/div[2]/div[1]/div/div/div[1]/div/a/div[2]/div[1]/p[2]")).getText();

        } catch (Exception ignored) {
        }
        Thread.sleep(1000);
        String time = "нет описания";
        try {
            time = driver.findElement(By.xpath("//div/div/div[1]/div/a/div[2]/div[2]/p")).getText();

        } catch (Exception ignored) {
        }

        return new Order(name, price, description, time);
    }

    private String getNumber(String chat) {
        System.out.println(chat);
        return in.nextLine();
    }
}
