import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Order {
    private String name;
    private String price;
    private String description;
    private String time;
}
